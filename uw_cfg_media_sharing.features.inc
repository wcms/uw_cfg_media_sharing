<?php

/**
 * @file
 * uw_cfg_media_sharing.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_cfg_media_sharing_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
